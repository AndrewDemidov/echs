﻿function keystrokeFromEvtData(/*web-browser keyboard event*/event) {
 var keyModifs = '', keyName = null;
 if (event.altKey) keyModifs += 'alt_';
 if (event.ctrlKey) keyModifs += 'ctrl_';
 if (event.metaKey) keyModifs += 'meta_';
 if (event.shiftKey) keyModifs += 'shift_';
 
 if (event.key) keyName = event.key;
 else if (event.keyIdentifier && event.keyIdentifier.substring(0, 2) !== 'U+') keyName = event.keyIdentifier;
 //else keyName =...;translate key code to key name.
 
 if (!keyName) return null;
 else return keyModifs + keyName.toLowerCase();
 }

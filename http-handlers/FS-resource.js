var dels, echo;

function FSresource(params) {
 //Params:
 //	the requester guesses, that the correct underlying type is "utype";
 //	the responder should roll with it.
 //	
 //	If "utype" is "dir", there are different meanings as to what will be returned:
 //	contents (entries), namesakes or an index file data.
 //	This is governed by "payload" param..
 //	"offerDownload" should be taken into account and a single redirChoice -- passed to dirViewer.
 //	
 //	If "utype" is "file", "cd" will govern the content disposition.
 //	A master page could be specified.
 
 if (!params) params = {};
 if (!params.utype) params.utype = 'dir';
 if (!params.payload) params.payload = 'index'; //or: 'entries', 'namesakes'
 //namesakes may be requested always or only in case of failure. Those are different namesakes!
 if (!params.offerDownload) params.offerDownload = false;
 if (!params.index) params.index = 'index';
 
 var propNames = ['utype', 'payload', 'index', 'offerDownload'];
 var propName;
 for (var propNamei = 0; propNamei < propNames.length; propNamei++) {
  propName = propNames[propNamei];
  this[propName] = params[propName];
  }
 this.headers = dels.makeMethodContainer("_");
 this.body = dels.makeMethodContainer("_");
 }

exports = module.exports = FSresource.prototype;

exports.src = function() {};
exports.srcs = function() {return []};

exports.handler = function(httpRq, httpRsp) {
 var fsr = this.getInstance();
 var handler = {};
 handler.GET = function(httpRq, httpRsp) {
  //this.getInstance();
  return getter(true);
  };
 handler.HEAD = function() {
  //this.getInstance();
  //return a func., that does everything handler.GET()() does, but set exports.body().sendBody()'s return value to false.
  return getter(false);
  };
 
 function getter(bSendBody) {
  //if (bSendBody === false) {
   if (fsr.body instanceof Function === false) fsr.body = dels.makeMethodContainer("_");
   fsr.body._.sendBody = dels.closure(bSendBody);
   //}
  return reprGetter;
  }
 
 var oUrl = require('url').parse(httpRq.url), pairs, nameValue, qName, qVal;
 
 //state machine:
 var initialState, utype, payload, indexFileName, bAppendExtension = true;
 var rsrcIdsFromState = {}; // = {entries: rsrcId};
 var contDisp;
 
 echo("Query string: '"+oUrl.query+"'.");
 if (typeof (oUrl.query) === 'string') {
  pairs = oUrl.query.split('&');
  for (var i = 0, l = pairs.length; i < l; i++) {
   nameValue = pairs[i].split('=');
   qName = nameValue[0];
   qVal = nameValue[1];
   if (qName === 'utype') utype = qVal;
   else if (qName === 'payload') payload = qVal;
   else if (qName.toLowerCase() == 'cd') {
    contDisp = qVal.toLowerCase();
    }
   }
  }
 if (!utype) utype = fsr.utype;
 if (!payload) payload = fsr.payload;
 
 //ignore query string and only read from settings:
 if (!indexFileName) indexFileName = fsr.index;
 
 initialState = payload;
 
 console.log("\r\nFSresource: in .handler: will create a DOOW...");
 var doow = new (
  require('mvcModelWrappers/DelegateOrObjectWrapper')
  )
 (
  [{}, {}, {}, 'get'],
  false,
  {httpRq: httpRq, httpRsp: httpRsp, id: '(fictive SFP)'}
  );
 
 var pathMod = require('path'), allowNodeRequireFor = {};
 
 console.log("\r\nFSresource: in .handler: created a DOOW...");
 
 return handler;
 
 function reprGetter(rsrcIdParts, httpRq, httpRsp, trueSfp) {
  //handles GET/HEAD.
  return runGetterStateMachine(rsrcIdParts[0]);
  function runGetterStateMachine(rsrcIdPart0) {
   'use strict';
   //Takes:
   //rsrcIdPart0 is the initial first part of a resource id.
   //
   //Does:
   //reads dirs or files, implementing HTTP GET's complex logic, that is guided by a state machine.
   //
   //Returns:
   //a Promise to prepare the soon-to-be-sent response's data, that is to store them in this RSRH instance.
   //The Promise will resolve to an object that represents the response's data.
   
   //maybe return HTTP error...
   //determine, whether to redirect...
   //or send success...
   //
   //if bSendBody true, make decision, otherwise dare not!
   //
   //Finally determine response writer.
   //'Tis unknown, what rsrcId is. All that's known is, it's a file system object.
   //What is its repr. and its "drag-n-drop" handler, is up to us now.
   
   var promPrepResp;
   
   var rsrcId = pathMod.join(rsrcIdPart0, rsrcIdParts[1]);
   //No, that was not a mistype.
   
   rsrcIdsFromState.entries = rsrcId;
   rsrcIdsFromState.index = pathMod.join(rsrcId, indexFileName);
   
   rsrcId = rsrcIdsFromState[payload];
   
   var iDecodedTail = rsrcIdParts[1].indexOf('..'), ixnay;
   if (iDecodedTail > -1) {
    if (
     (
      iDecodedTail === 0
      || /[\/\\]/.test(
       rsrcIdParts[1][iDecodedTail-1]
       )
      )
     && (
      rsrcIdParts[1].length === 2
      || /[\/\\]/.test(
       rsrcIdParts[1][iDecodedTail+2]
       )
      )
     )
    {
     //possibility of an attack: trying to access files with '..' hack.
     payload = 'ixnay';
     ixnay = {code: 403, payloadType: 'nullDevice'};
     }
    //otherwise it's just two dots in a name.
    }
   
   if (utype === 'dir') {
    if (payload == 'index') {
     //rsrcIdStack.push(rsrcId);pathMod.join(rsrcId, indexFileName);
     promPrepResp = readAsFile(rsrcId);
     promPrepResp = promPrepResp.catch(handleFileErrors);
     }
    else if (payload === 'entries') {
     promPrepResp = readAsDir(rsrcId);
     promPrepResp = promPrepResp.catch(handleDirErrors);
     }
    else {
     //payload === 'namesakes' or whatever.
     promPrepResp = Promise.resolve(ixnay); //later.
     }
    
    promPrepResp = promPrepResp.then(finalise, finalise);
    return promPrepResp;
    }
   else //utype=file
   {
    return readAsFile();
    }
    
   function finalise(resp) {
    //Takes:
    //resp is an object with:
    //	payloadType -- string;
    //		registered payload types: 'file', 'dirEntries', 'nullDevice'
    //		and 'error'.
    //	payload or payloadRef -- content or stream;
    //	code -- HTTP status code;
    //	headers -- a "request-specific handler" (methods instead of props);
    //
    //Does:
    //
    //
    //Returns:
    //resp.
    
    echo(
     'Finalising HTTP response...\r\nReceived: '
     + (
      require("objectToString")
      (arguments[0])
      )
     );
    
    if (resp.payloadType === 'error')
    return (resp.finaliseHttpResponsePreparation = finalise, Promise.reject(resp));
    
    echo("Final rsrcId = '"+rsrcId+"', final payload = '"+payload+"'.");
    
    //set code, headers and response writer...
    fsr.code = dels.closure(resp.code);
    
    //directories determine their own content type, and files do so based on their extension.
    //If, for some reason, they haven't determined it, we set a default below:
    
    var ct, ctf;
    ctf = (resp.headers) && (resp.headers['content-type']);
    //optional .headers is a "request-specific handler",
    //meaning, it has methods.
    //ctf = fsr.headers()['content-type'];
    
    if (ctf instanceof Function) ct = ctf();
    else ct = '';
    
    echo("Would set Content-Type to '"+ct+"'.");
    
    //if none:
    if (
     (typeof ct) !== 'string'
     && ct instanceof String === false
     || ct.trim() === ''
     )
    fsr.headers()['content-type'] = dels.closure('text/html');
    
    else fsr.headers()['content-type'] = dels.closure(ct);
    
    echo("Have set Content-Type to '"+(fsr.headers()['content-type']())+"'.");
    
    //also set other headers...
    
    var trueHandler = fsr; //alias.
    
    var wrw;
    if (trueHandler.body().writeResponseWith instanceof Function === false) {
     trueHandler.body().writeResponseWith = function() {};
     }
    wrw = trueHandler.body().writeResponseWith();
    if (false === wrw instanceof Array) {
     wrw = [];
     trueHandler.body().writeResponseWith = dels.closure(wrw);
     }
     
    echo("\r\nStatus code: "+resp.code+".");
    
    //must determine response writer. wrw[0] may be anything.
    if (resp.code == 404) {
     echo('not found.');
     //code is set above.
     //fsr.code = dels.closure(404);
     (fsr.headers())['content-type'] = dels.closure("text/plain; charset=utf-8");
     wrw[1] = function(inexistentRelPath, unused, httpRsp, trueSfp) {
      return new Promise(function(onIoPerformed, onError) {
        var uri = require('path').posix.join(trueSfp.firstKey, trueSfp.firstKeyTail);
        if (uri != trueSfp.firstKey)
        httpRsp.write("'"+uri+"' not found, try '"+trueSfp.firstKey+"' instead.");
        else httpRsp.write("'"+uri+"' not found [sad smiley face].");
        onIoPerformed(null);
        }
       );
      };
     }
    else if (resp.code == 403) {
     echo("forbidden.");
     //code is set above.
     //trueHandler.code = dels.closure(403);
     (trueHandler.headers())['content-type'] = dels.closure("text/plain; charset=utf-8");
     wrw[0] = 'utf-8'; //encoding isn't used.
     wrw[1] = function(filePath, encod, httpRsp, trueSfp) {
      return new Promise(
       function(onIoPerformed, onError) {
        httpRsp.write("'" + trueSfp.firstKey + trueSfp.firstKeyTail + "': nope!\r\n");
        httpRsp.write("'"+trueSfp.firstKey+"' should work, though.");
        onIoPerformed(null);
        }
       )
      };
     
     //return Promise.resolve(null); //why? 'null' is returned below in any case!
     }
     
    //maybe, later?
    /*else if (resp.code >= 300 && resp.code <= 399) {
     //not forbidden, redirect...
     }
    */
    
    else if (resp.code >= 300 && resp.code <= 399 || resp.code >= 200 && resp.code <= 299) {
     var viewer;
     
     if (resp.payloadType === 'dirEntries') {
      viewer = require('dirViewer');
      console.log(
       "\r\nRequired module 'dirViewer'. The module is: "+(require('type').typeNameOf(viewer))+"."
       );
      }
     else if (resp.payloadType === 'file') {
      //content-disposition.
      if (contDisp == 'attach' || contDisp == 'attachment')
      fsr.headers()['content-disposition']
      = dels.closure('attachment; filename="'+pathMod.basename(resp.payloadRef.path)+'"');
      
      else fsr.headers()['content-disposition']
      = dels.closure('inline');
      
      echo("Set 'content-disposition' to '"+(fsr.headers()['content-disposition']())+"'.");
      
      viewer = require('serverSideDownloader');
      echo('Required module "serverSideDownloader".');
      }
     
     var vinst;
     vinst = new (viewer.constructor)(
      //this may not have any effect on other 'resource-specific request handlers':
      {showNamesakeFiles: payload == 'myNamesakes'}
      //the parametres should be passed in resp.mvcViewProducerParams.
      );
     
   // = require('type').inheritDeeply(dirViewer);
   //if (dvinst.constructor instanceof Function)
   //dvinst.constructor({showNamesakeFiles: fsr.payload == 'namesakes'});
     
     console.log("Will make usfp...");
     //we've changed (DOOW).makeUsfp(), node modules are expected to be at index 1!
     var usfp = doow.makeUsfp([null, vinst]);
     console.log("\r\nFSresource: in .handler.GET (or ...HEAD): made usfp.");
     var mvcViewProducer = usfp.get("mvcViewProducer");
     wrw[0] = resp;
     wrw[1] = mvcViewProducer;
     resp.mvcViewProducerParams = {showNamesakeFiles: payload === 'myNamesakes'};
     (function(relPathToEntry, responseData, httpRsp, trueSfp) {
       responseData.payload || responseData.payloadRef
       httpRsp.write();
       });
     }
    
    else {
     //code 500
     if (resp.code > 599) fsr.code = dels.closure(500);
     wrw[1] = function() {return Promise.reject(resp instanceof Error? resp: resp.payload)};
     }
    
    return resp; //'HTTP method implementations' may return Promises resolving to anything. For chaining we need this.
    } //here ends 'finalise()'.
   
   function filterDirEntries(globPattern) {
    return function(resp) {
     //{code: 200, payloadType: 'dirEntries', payload: listedEntries}
     var listedEntries, filteredEntries = [];
     if (resp.code >= 200 && resp.code <= 299 && resp.payloadType === 'dirEntries') {
      listedEntries = resp.payload;
      //now filter out entries matching globPattern:
      for (var i = 0, l = listedEntries.length; i < l; i++) {
       if (listedEntries[i].indexOf(globPattern) === 0) filteredEntries.push(listedEntries[i]);
       }
      //if no choices, then file error, append no exts.
      if (filteredEntries.length < 1) {
       bAppendExtension = false;
       return handleFileErrors({code: 'ENOENT'});
       }
      //else if one choice, read that file.
      else if (filteredEntries.length < 2) {
       bAppendExtension = false;
       return readAsFile(
        pathMod.join(rsrcId, filteredEntries[0])
        )
       .catch(handleFileErrors);
       }
      //else pretend to have read a dir, whose contents are the choices.
      else return {code: 300, payloadType: 'dirEntries', payload: filteredEntries};
      }
     else return resp;
     };
    }
   
   function handleFileErrors(er) {
    //can't read as file, maybe dir?
    try {
    echo("\r\ncan't read as file, maybe dir?");
    echo("rsrcId = "+rsrcId+".");
    echo("utype = "+utype+", payload = "+payload+".");
    //echo("bAppendExtension = "+bAppendExtension+".");
    //echo("Popato.");
    //echo("bAppendExtension = "+bAppendExtension+".");
    echo("er.code = "+(er.code)+".");
    echo("bAppendExtension = "+bAppendExtension+".");
    }
    catch(er) {return Promise.reject(er)}
    if (utype === 'dir' && payload === 'index') {
     if (er.code === 'EISDIR' || er.code === 'ENOENT') {
      if (bAppendExtension) {
       //rsrcId = rsrcIdStack.pop();
       payload = 'indexNamesakes';
       //rsrcId = rsrcIdsFromState[payload];
       //if (!rsrcId)
       rsrcId = rsrcIdsFromState[payload] = rsrcIdsFromState.entries;
       return readAsDir(rsrcId).then(filterDirEntries(indexFileName), handleDirErrors);
       }
      else {
       echo("attempt 'payload=entries':");
       //rsrcId = rsrcIdStack.pop();
       payload = 'entries';
       rsrcId = rsrcIdsFromState[payload];
       //maybe, rsrcId is no dir. Should try to read as a file and, maybe, append ext.:
       bAppendExtension = true;
       return readAsDir(rsrcId).catch(handleDirErrors);
       //or, maybe, show namesakes.
       }
      }
     else {
      //file unreadable for some reason...
      echo("file unreadable for some reason...");
      return '400 Unreadable for some reason';
      }
     }
    else if (utype === 'dir' && payload === 'indexNamesakes') {
     if (er.code === 'ENOENT' || er.code === 'EISDIR') {
      echo('Appending extension to index file name failed.');
      echo('Will attempt "payload=entries".');
      payload = 'entries';
      rsrcId = rsrcIdsFromState[payload];
      bAppendExtension = true;
      return readAsDir(rsrcId).catch(handleDirErrors);
      }
     }
    else if (utype === 'dir' && payload === 'myNamesakes') {
     //handle payload=myNamesakes here.
     //append no extension.
     //file inexistent.
     
     //handle this case with searchUpPathAndWrite(), which catches errors
     //and, well, searches up path for us:
     var allow = allowNodeRequireFor[rsrcIdParts[1]];
     
     if (allow == null) {
      trueSfp.traverseKeysInSfpChain(['body', 'allowNodeRequire'], -1, false);
      var anr = trueSfp.current.unwrap();
      if (anr != null)
      allow
      = allowNodeRequireFor[rsrcIdParts[1]]
      = (
       new (
        require('server/settingsPerSite').SettingsForPath
        )
       (anr, rsrcIdParts[1])
       )
      .current.myValue;
      if (allow == null) allow = allowNodeRequireFor[rsrcIdParts[1]] = false;
      }
     
     var absPath = pathMod.resolve(pathMod.join(require('echs/server').getSiteRootDir(), rsrcIdPart0));
     //pathMod.isAbsolute()
     if (allow === true && absPath != pathMod.join(absPath, '..'))
     {
      echo("allow Node require() behaviour.");
      payload = initialState;
      //rsrcId = rsrcIdsFromState[payload] = pathMod.join(rsrcId, '..');
      return runGetterStateMachine(pathMod.join(rsrcIdPart0, '..'));
      }
     
     else {
      echo("disallow Node require() behaviour.");
      return Promise.resolve(
       {payloadType: 'error', payload: {msg: 'may not use Node require() behaviour, will try next src'}}
       );
      }
     
     }
    
    else return Promise.reject(er);
    }
   
   function handleDirErrors(er) {
    //can't read as dir, maybe file?
    echo("\r\ncan't read as dir, maybe file?");
    echo("rsrcId = "+rsrcId+".");
    echo("utype = "+utype+", payload = "+payload+".");
    echo("er.code = "+(er.code)+".");
    echo("bAppendExtension = "+bAppendExtension+".");
    if (utype === 'dir' && payload === 'entries') {
     //if no entry, maybe, append extension?
     //no append or no found with ext. -- maybe cd ..?
     if (er.code === 'ENOENT') {
      if (bAppendExtension) {
       //rsrcIdStack.push(rsrcId);
       //rsrcId = pathMod.join(rsrcId, '..')
       
       payload = 'myNamesakes'; //must avoid infinite looping somehow...
       //rsrcId = rsrcIdsFromState[payload];
       
       //if (!rsrcIdsFromState[payload])
       rsrcIdsFromState[payload] = pathMod.join(rsrcId, '..');
       rsrcId = rsrcIdsFromState[payload];
       return readAsDir(rsrcId)
       //.then(function() {rsrcId = prevRsrcId; return arguments[0]})
       .then(
        filterDirEntries(
         pathMod.basename(rsrcIdsFromState.entries)
         ),
        handleDirErrors
        )
       }
      else return {code: '404', payloadType: 'nullDevice'};}
     else if (er.code === 'ENOTDIR') return readAsFile(rsrcId);
     }
    else if (utype === 'dir' && payload === 'indexNamesakes') {
     if (er.code === 'ENOENT') {
      //probably can't read dir with namesakes.
      //it's supposedly inexistent.
      //attempt payload=entries; but the dir. is seemingly absent!
      //So handle it recursively [shudder-shudder]:
      
      //rsrcId = rsrcIdStack.pop();
      payload = 'entries';
      rsrcId = rsrcIdsFromState[payload];
      bAppendExtension = true;
      return handleDirErrors(er);
      }
     else if (er.code === 'ENOTDIR') {
      //rsrcId = rsrcIdStack.pop();
      rsrcId = rsrcIdsFromState[payload];
      return readAsFile(rsrcId);
      }
     }
    else if (utype === 'dir' && payload === 'myNamesakes') {
     //regardless of error code this final stage has failed.
     //No namesakes.
     return handleFileErrors(er);
     //{code: 404, payloadType: 'nullDevice'};
     }
    else return Promise.reject(er);
    }
   
   function readAsDir(dirPathRel) {
    'use strict';
    //(sFilePath, sEncoding, httpRsp, trueSfp, trueHandler)
    //echo("\r\ndirViewer: inside handler().");
    
    echo("\r\nreadAsDir('"+dirPathRel+"'): Applying HTTP method.");
    echo(
     "Source: "+dirPathRel
     //+", encoding: "+sEncoding
     //+", SFP: SFP#"+trueSfp.id+": "+require('objectToString')(trueSfp)
     +""
     );
    
    var trueHandler = fsr; //alias.
    
    var siteRootDir = require('echs/server').getSiteRootDir();
    var fsMod = require('fs');
    
    var actualPath = pathMod.join(siteRootDir, dirPathRel);
    echo("Actual path: " + actualPath);
    echo("Site's root dir: '"+pathMod.join(siteRootDir, '.')+"'.");
    var i = actualPath.indexOf(pathMod.join(siteRootDir, '.'));
    echo("Index of site's root dir in actual path: "+i+".");
    
    var wrw;
    if (typeof (trueHandler.headers) !== "function") trueHandler.headers = dels.makeMethodContainer("_");
    if (trueHandler.body().writeResponseWith instanceof Function === false) {
     trueHandler.body().writeResponseWith = function() {};
     }
    wrw = trueHandler.body().writeResponseWith();
    if (false === wrw instanceof Array) {
     wrw = [];
     trueHandler.body().writeResponseWith = dels.closure(wrw);
     }
     
    if (i < 0 && allowNodeRequireFor[rsrcIdParts[1]] == false) {
     //actualPath matches not site's root dir..
     //allow Node require() behaviour?
     //(should cache the answer)
     
     return Promise.resolve({code: 403, payloadType: 'nullDevice'});
     }
     
    else {
     //not forbidden.
     echo("Will try to read dir '"+actualPath+"'.");
     //if (showNamesakes) echo("(If there are any namesakes, will list them for choosing.)");
     
     //examine a query param. and decide, whether actualPath is rather a dir. or a file.
     //If it's a dir., it can be viewed in 3 ways: listing of entries, showing namesakes or displaying
     //an index file (such as index.html).
     //If it's a file, there is an option to offer a download or not.
     //A master page could be served...
     
     //if (trueHandler.utype == 'dir')
     return new Promise(
      function(onDirRead, onReadingFailed) {
       fsMod.readdir(
        //(showNamesakes? pathMod.dirname(actualPath): actualPath),
        actualPath,
        {},
        function(er, fileNames) {
         if (er instanceof Error) onReadingFailed(er);
         else onDirRead(fileNames);
         }
        );
       }
      )
     //.then(setDirContentsWriter/*, onReadingFailed*/);
     //finalisation:
     .then(
      function(listedEntries) {
       var resp = {code: 200, payloadType: 'dirEntries', payload: listedEntries};
       return resp;
       }
      );
     
     //else //should be 'utype=file'.
     //return Promise.resolve(null);
     }
    
    }
   
   function readAsFile(filePathRel) {
    'use strict';
    //Returns:
    //Promise to open read stream for filePathRel and to wait for when may read.
    
    echo("\r\nreadAsFile('"+filePathRel+"'): Applying HTTP method.");
    
    var siteRootDir, underFileReadStream, fullPathToFile;
    var headers, rWrw;
    var mimeInfo;
    
    siteRootDir = require('echs').getSiteRootDir();
    fullPathToFile = pathMod.join(siteRootDir, filePathRel);
    mimeInfo = require('server/files').mimeFromFilePath(fullPathToFile);
    
    headers = {};
    headers['content-type'] = dels.closure(
     mimeInfo.type+'/'+mimeInfo.subtype
     +(mimeInfo.charset? ('; charset='+mimeInfo.charset): '')
     );
    rWrw = [];
    rWrw[0] = mimeInfo.charset; //usfp.get('writeResponseWith');
    
    //debug:
    //rWrw = ['utf-8'];
    
    return new Promise(function(setResponseWriter, onFileReadFailed) {
      echo("Will attempt read-stream creation for '"+fullPathToFile+"'.");
      underFileReadStream = require('fs').createReadStream(
       fullPathToFile,
       {
        flags:'r', encoding:rWrw[0], autoClose: true, start: 0
        }
       ); //here ends call to .createReadStream().
      underFileReadStream.once('readable', setResponseWriter);
      underFileReadStream.on('error', onFileReadFailed);
      }
     )
    .then(
     function() {
      return {code: 200, payloadType: 'file', payloadRef: underFileReadStream, headers: headers}
      }
     );
    
    }
   
   }
  }
 function reprSetter() {
  //handles PUT.
  }
 
 };

dels = require('server/delegates');
//exports.code = function(rq, rsp) {return this.dvinst.code(rq, rsp)};
//exports.headers = function() {return this.dvinst.headers()};
exports.headers = dels.makeMethodContainer("_");
exports.body = dels.makeMethodContainer("_");

echo = require('devTools').echo;
var doPromiseToPipe = require('devTools').promiseToPipe;
var otos = require('objectToString');

if (require.main === module) {
 let sources =
 //['inexistent']; //test passes.
 //['package']; //uniqueFileNameLacksExt, test passes.
 //['server']; //multipleExtsMatchThisFileName, test passes; no butchered links;
 //reports correct dir. to dirViewer.
 //['package.json']; //a case of exact file-name-matching. Test passes.
 ['site']; //a case of existing dir. Test passes.
 
 let fsr = new FSresource(
  {
   index:
   //'inexistentIndex' //test passes.
   //'indexLacksExt' //test passes.
   //'h' //['site/multipleExtsMatchThisFileName']; //test passes.
   'existingIndexFile.txt' //test passes.
   }
  )
 , rq = {url: '/'} //?payload=entries
 , rsp = new (require('DuplexStream'))();
 
 let origWrite = rsp.write;
 rsp.write = function(chunk) {
   echo('\r\nPretending to write chunk: '+otos(chunk)+'');
   origWrite.call(this, chunk);
   };
 rsp.writeHead = function(code, headers) {
  echo('\r\nPretending to write code: '+otos(code)+', headers: '+otos(headers)+'.');
  };
 
 fsr.getInstance = function() {return this};
 let httpGet = fsr.handler(rq, rsp).GET(rq, rsp)
 , sfp = new (
  require('server/settingsPerSite').SettingsForPath
  )
 (
  {
   '/': {src: '.', body: {allowNodeRequire: {'quasiNodeModule': true}}}
   },
  '/'
  //'/quasiNodeModule'
  )
 ;
 
 sfp.httpRq = rq;
 sfp.httpRsp = rsp;
 
 echo('\r\nMay the test begin.');
 /*require('server/files').searchUpPathAndWrite(
  sources,
  sfp,
  rsp //response
  ,rq //request
  ,httpGet
  )
 */
 httpGet([sources[0], sfp.firstKeyTail], rq, rsp, sfp)
 .then(
  function() {
   echo('\r\nWould be setting code now...');
   echo('Instead will pretend to write response.');
   var writer, writerArg, wrw;
   wrw = fsr.body(rq, rsp).writeResponseWith(rq, rsp), writerArg = wrw[0], writer = wrw[1];
   //require('server/files').searchUpPathAndWrite(sources, sfp, rsp, writerArg, writer)
   writer([sources[0], sfp.firstKeyTail], writerArg, rsp, sfp)
   .then(
    function(er) {
     if (er == null) echo("\r\nHas seemingly written response.");
     else echo("\r\nResolved to value "+otos(er)+", will result in 'HTTP 500'.");
     }
    );
   }
  ,function(er) {
   echo("\r\nOh my... What's that?..");
   echo(er && er.toString());
   echo(otos(er));
   echo("Seemingly no sources, would have attempted another one...");
   }
  );
 }

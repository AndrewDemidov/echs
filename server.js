﻿
//nodejs, v8.
var pathMod = require('path');

var
serverRootDir = pathMod.dirname(module.filename), //pathMod.join(process.argv[1], '..'),
defaultPort = 3001, port = process.argv.length > 2 ? process.argv[2] : defaultPort;
//site's root dir.:
var rootDir;
var settingsName = 'server.json', settingsDir, settings;
var delegateCache = {}, sourcesCache = {}, sfpNullishPropsCache = {};
//see serveRq().

var devTools = require('devTools'),
echo = devTools.echo,
promiseToPipe = devTools.promiseToPipe;

if (String(port).toLowerCase() === "help") {
 echo("Usage:\r\nnode server.js portNumber lookUpLocalFilesIn settingsDir"
  + "\r\n\r\nportNumber: server listens to connections to this port. Defaults to '"+defaultPort+"'."
  + "\r\n\r\nlookUpLocalFilesIn: references to local files, whenever made, are relative to this cmd option. Defaults to '"+serverRootDir+"'."
  + "\r\n\r\nsettingsDir: where to look for the settings file. Defaults to lookUpLocalFilesIn option.");
 return;
 }

echo('serverRootDir set to: "' + serverRootDir + '".');

module.exports = {
 shutDown: function() {
  echo("Is server listening? "+server.listening+"");
  if (server.listening) {
   echo("Closing, releasing TCP port...");
   echo("No new connections will be accepted.");
   echo("Will I exit, once it be closed?..");
   }
  server.close(function() {
    echo("\r\nClosed all connections, released port "+port+".");
    echo("Callback's args: "+otos(arguments)+".");
    }
   );
  },
 setupSite: function(portNumber, lookUpLocalFilesIn, settingsDir) {
  port = portNumber;
  if (!port || String(port).toLowerCase() === 'default') port = defaultPort;
  //maybe, invoke help.
  rootDir = lookUpLocalFilesIn;
  if (!rootDir) rootDir = serverRootDir;
  echo('rootDir set to: "' + rootDir + '".');
  if (!settingsDir) settingsDir = rootDir;
  echo("Fetching settings from server.json...");
  return new Promise(
   function(onSettingsRetrieved, onBroke) {
    'use strict';
    var s = '';
    //may specify settings directory, but not its file's name yet.
    var readfrom = fs.createReadStream(
     pathMod.join(settingsDir, settingsName),
     {
      flags:'r', encoding:'utf8', //encoding should be changed to ANSI
      autoClose: true, start: 0
      }
     );
    readfrom.on('data', function(data) {s += data});
    readfrom.on('end', function() {
      try {settings = JSON.parse(s)}
      catch(er) {echo("\r\nCan't parse the following JSON:");
       echo(s);
       echo("because of: "+er+"!");
       onBroke(er);
       }
      finally {onSettingsRetrieved(settings)}
      }
     );
    readfrom.on('error', function(er) {
      echo("\r\nCan't read settings!");
      echo(er);
      echo("\r\nWill assume existence of a file with a default object.");
      settings = {};
      onSettingsRetrieved(settings);
      //onBroke(er);
      }
     );
    }
   )
  
  //settings retrieved:
  .then(onSettingsRetrieved)
   
  function onSettingsRetrieved(settings) {
   //rootDir may change here acc. to settings.
   require('server/delegates').delegateCache = {};
   //must purge cache on a new site establishment.
   process.on('uncaughtException', function() {
     echo("\r\nCould not catch exception: "+otos(arguments)+"!");
     module.exports.shutDown();
     }
    );
   process.on('SIGINT', function() {
     echo("\r\nReceived SIGINT (interrupt), callback's args: "+otos(arguments)+"");
     module.exports.shutDown();
     }
    );
    
   //----
try {
 secureOptions.key = fs.readFileSync(pathMod.join(rootDir, './testPrivKey.pem'));
 secureOptions.cert = fs.readFileSync(pathMod.join(rootDir, './testCert.pem'));
 }
catch (e) {echo("\r\nWill not establish a proper secure server due to: " + e.toString())}

server = new Server(secureOptions);

//Events http.Server has: newListener, removeListener, close, connection, error, listening,
//checkContinue, checkExpectation, clientError, connect, request.

//Additionally https.Server has the following: keylog (in new versions of Node), newSession,
//OCSPRequest, resumeSession, secureConnection, tlsClientError.

for (
 var eventName in {
  'newListener': true, 'removeListener': true, 'close': true, 'connection': true, 'error': true,
  'listening': true, 'checkContinue': true,
  'checkExpectation': true, 'clientError': true, 'connect': true, 'request': true
  }
 )
server.on(
 eventName,
 (
  function(eventName) {
   return function eventHandler() {
    console.log(
     "\r\nOn event '" + eventName + "'"
     + "."
     //+ ": args: " + otos(arguments, ' ', 1)
     );
    }
   }
  )
 (eventName)
 );

server.on('request', requestHandlerWrapper);

   //----
   server.insecureServer.listen(port);
   server.secureServer.listen(Number(port) + 1);
   echo('Server is listening at port '+port+'.');
   echo('And probably at port '+((port - 0) + 1)+' too.');
   
   if (!settings['/favicon.ico'])
   settings['/favicon.ico'] = {code: 204, headers: {'content-type': 'image/vnd.microsoft.icon'}, body: {}};
   
   if (!settings['/']) settings['/'] = {};
   if (!settings['/'].src) settings['/'].src = '.'; //probably unwise, but whatever.
   
   if (!settings.default) settings.default = {};
   
   //if (!settings.default.delegate) settings.default.delegate = {};
   
   //populate with methods this server supports:
   //if (!settings.default.delegate['*']) settings.default.delegate['*'] = {};
   if (!settings.default.handler)
   settings.default.handler = "http-handlers/FS-resource";
   
   modServerFiles.settings = settings; //import settings into modServerFiles.
   //modServerFiles.rootDir = rootDir; //also import rootDir.
   } //end of onSettingsRetrieved().
  }, //end of .setupSite().
 
 writeFromToSafely: writeFromToSafely,
 sendWebPage: sendWebPage,
 putContentIntoFile: putContentIntoFile,
 
 getServerRootDir: function() {return serverRootDir},
 getSiteRootDir: function() {return rootDir || this.getServerRootDir()}
 
 }; //here ends module.exports.

//It seems, as though .finally is not defined!
if (typeof Promise.prototype.finally !== 'function')
Promise.prototype.finally = function(onBoth) {
 return this.then(onBoth, onBoth);
 };

var fs = require('fs');
var httpMod = require('http'), Server = require('server/Server');

var secureOptions = {}, server;

var modServerFiles = require('server/files.js')(rootDir, settings),
searchUpPathAndWrite = modServerFiles.searchUpPathAndWrite;

//module.exports is ready to be used.

echo('\nYay, this works!');

//if server.js is rather executed directly, than require()-d:
if (require.main === module) {
 rootDir = process.argv.length > 3 ? process.argv[3] : serverRootDir;
 settingsDir = process.argv.length > 4 ? process.argv[4] : rootDir;
 module.exports.setupSite(port, rootDir, settingsDir);
 }

//console.log('\nmodule.filename: ' + module.filename);
//console.log('Must reside in dir: ' + pathMod.dirname(module.filename));
//return;

var objToString = require('objectToString'),
otos = objToString;

var mergeFiles = require('merge');

//Request's payload can be read from request's object (rq):
//rq.on('data', ...), rq.on('readable', ...)
//or: rq.pipe(writable, options);
//
//Check rq's content-length and content-type (via rq.headers object). also 'origin' and 'referer'.
//Content-length tells payload's length, content-encoding specifies encoding algorithmes applied to the payload, content-type tells, how to interpret payload after decoding (since it's sent as text).

function requestHandlerWrapper(rq, rsp) {
 //require('url').parse('http://username:password@domain.name:80/pa/th?p1=v1&p2=v2#id') -->
 //{protocol: 'http:', auth: 'username:password',
 //host: 'domain.name:80', hostname: 'domain.name',
 //port: '80', path: '/pa/th?p1=v1&p2=v2',
 //pathname: '/pa/th', search: '?p1=v1&p2=v2',
 //query: "p1=v1&p2=v2", hash: '#id',
 //href: '<reference as whole>', slashes: true}
 
 echo("\r\n"+rq.method+" "+rq.url+" HTTP/"+rq.httpVersion+"");
 echo("Host: " + rq.headers.host);
 echo('User-Agent: '+rq.headers['user-agent']+'.');
 for (var name in rq.headers) {
  if (name != 'host' && name != 'user-agent') echo(""+name+": "+rq.headers[name]+"");
  }
 //echo("\r\nRequest: " + otos(rq));
 
 try {serveRq(rq, rsp).finally(respondInAnyCase);}
 catch (er) {respondInAnyCase(er)}
 
 function respondInAnyCase(er) {
  echo("\r\n");
  //we might catch an error.
  if (er != null && false === er instanceof Error) er = new Error(er);
  if (er instanceof Error) {
   echo("WHY NO ONE catches errors?!");
   echo(er.toString());
   //this is not supposed to happen. It would be a successful response, if not for this error.
   //The server has erred:
   if (rsp.statusCode < 500 || rsp.statusCode > 599) rsp.statusCode = 500;
   rsp.setHeader('Content-Type', 'text/plain; charset=UTF-8');
   rsp.write('An uncaught error of type: '+er.constructor.name+'.');
   }
  else {
   //console.log('Ready to respond with:\r\n' + otos(rsp));
   }
  echo("Thus ends server's response to request '"+rq.method+" "+rq.url
   +" HTTP/"+rq.httpVersion+"'.");
  echo('Responding: HTTP/1.1 '+rsp.statusCode+' '+rsp.statusMessage
   +'.'
   );
  rsp.end();
  echo('Responded: HTTP/1.1 '+rsp.statusCode+' '+rsp.statusMessage
   +'.'
   );
  }
 }
 
//var Wrapper = require('mvcModelWrapper');
var SFPwrapper;

function
/*Promise to serve request rq and to write response to rsp*/
serveRq(rq, rsp) {
 'use strict';
 var oUrl = require('url').parse(rq.url);
 
 var sfp;
 var /*[String]*/srcs;
 var httpMethods, httpMethod, bAllowed = false;
 var headerNames, headerName, header, sendBody = true;
 var handler;
 var responseWriterArg, responseWriter, wrw;
 
 var fbkWrw;
 var bodyRules;
 echo("\r\nLet's serve this request!");
 
 //map request paths to responses in the future.
 //An object may be used: {'certain path': {responseCode: codeHere, headers: {}, body: {src: 'specify source', compile: {template: {/*template def. here*/}, rules: {add: {/*here go rules*/}, totalOverride: true || false}}}}}
 //A similar object might be used for defaults: default path, headers, template, rules etc..
 
 //debugger;
 
 var debugSettings = {
  debug: true,
  '/': {
   code: 200,
   headers: {'Content-Type': 'text/plain; charset=UTF-8'},
   body: {needMasterPage: false},
   delegate: {
    'GET': {handler: null}
    }
   }
  };
 
 //New plan.
 //Check, if method implemented and respond with 501 Unimplemented otherwise;
 if (rq.method == 'POST') {
  echo("\r\nPOST's payload:");
  return promiseToPipe(rq, process.stdout)
  }
 else if (rq.method !== 'GET' && rq.method !== 'HEAD' && rq.method !== 'PUT' && rq.method !== 'POST') {
  //It must be GET! If it is not, it is not implemented (501).
  //rsp.statusCode = 501;
  rsp.writeHead(501, {"Content-Type": "text/plain;charset=utf-8"});
  rsp.write("Request method '"+rq.method+"' is not supported. 'GET' or 'HEAD' should work.");
  return Promise.resolve(null);
  }
 
 //Create an SFP, validate its .current;
 
 //extract settings for path from the 'settings' object:
 sfp = new SettingsForPath(settings, oUrl.pathname);
 
 echo("\r\nserveRq(): created an SFP for server settings.");
 echo("sfp.parent should be the root (the settings object).");
 
 SFPwrapper = require('mvcModelWrappers/SFPwrapper');
 
 echo("\r\nLoading delegate from sfp...");
 
 sfp.httpRq = rq;
 
 //sfp.subwrappers[''] = 
 //require('mvcModelWrappers/SFPwithDelegateWrapper');
 //require('server/delegates').FakeSFPwithDelWrapper;
 
 sfp.httpRsp = rsp;
 echo("\r\n--------------------------------\r\nAssigned HTTP response object to SFP#"+sfp.id+".");
 
 //sfp.useValidSubwrapper(sfp.current);
 
 sfp.isNull = sfpNullishPropsCache[sfp.firstKey] || {};
 
 //throw "Aborting serving request for debug purposes...";
 echo("\r\nWill the server process settings correctly this time?");
 
 //Enumerate methods with (SFP).getKeys();
 
 //call .loadDelegateFromSettings() directly and use its retval.
 //Use sfp to get "handler", "handlerParams" and "src"/"srcs";
 //use underResource for other purposes.
 
 var underResource = require('server/delegates').loadDelegateFromSettings(sfp, sfp.httpRq);
 
 echo("\r\nWill get-handler.");
 underResource.traverseKeysInSfpChain([], -1, false); //cd to root.
 underResource.get("handler");
 echo("\r\nDone get-handler.");
 
 echo("\r\nWill enumerate-handler.");
 httpMethods = underResource.getKeys(), httpMethod, bAllowed = false;
 echo("\r\nDone enumerate-handler.");
 echo(
  "\r\nhttpMethods: "
  + (
   httpMethods instanceof Array && httpMethods.join(", ")
   )
  );
 echo("underResource.current is at: " + underResource.keysToMyCurrent().join(', '));
 
 //if (sfp.currentKey === 'handler' && sfp.current.myValue == null)
 
 //If the method requested is not on the list or results in a nonfunc, respond with 405 Unallowed;
 //(That is:)
 //method = sfp.get(rq.method);
 //if (method instanceof Function) searchUpPathAndWrite(unifySources(sfp), sfp, rsp, rq, method);
 //else rsp.writeHead(405, {"Allow": methodNames.join(", ")});
 
 for (var i = httpMethods.length - 1; !bAllowed && i > -1; i--) {
  if (httpMethods[i] === 'getInstance') httpMethods.splice(i, 1);
  //refuse and proceed to the next iteration.
  else if (httpMethods[i] == rq.method) {
   httpMethod = underResource.get(httpMethods[i]);
   if (httpMethod instanceof Function) bAllowed = true;
   else httpMethods.splice(i, 1);
   underResource.unget();
   }
  }
 
 if (bAllowed) {
  //Check preconditions, if any, and possibly fail;
  //...
  //On checked preconds pass the enumerated method implementation to searchUpPathAndWrite();
  //In the method implementation make use of closures or whatevs and get an instance of the delegate;
  //Assign the instance the status code and response headers, as the request is being served;
  //Then proceed with the current logic.
  
  //unget .handler:
  underResource.unget();
  if (sourcesCache[sfp.firstKey] instanceof Array) {
   srcs = sourcesCache[sfp.firstKey];
   echo(
    "\r\nHad cached sources for '"+sfp.firstKey+"': "+srcs.join(", ")+"."
    );
   }
  else {
   echo("\r\nWill unify-sources.");
   srcs = unifySources(
    sfp,
    [sfp.keys.length > 0? sfp.keys[0]: sfp.currentKey]
    );
   echo("\r\nDone unify-sources.");
   
   //in case sfp.firstKey has neither src, nor srcs of its own, they will match the fallbacks!
   //That may result in '/help' being mapped to 'site', even though mapped to 'site' is '/',
   //which means, '/help' will have to map to 'site/help'!
   
   //The sources have, thus, to line up with sfp.firstKey:
   srcs = /*['']*/lineUpSourcesWith( /*[{}]*/ srcs, /*''*/ sfp.firstKey);
   
   //cache:
   sourcesCache[sfp.firstKey] = srcs;
   }
  
  //sfp.firstKey is taken from the request URI and may be encoded!
  //Sources must be decoded!
  //sfp.firstKeyTail must be decoded as well!
  //They will all be decoded in applyHttpMethodToSources().
  }
 else {
  rsp.statusCode = 405;
  rsp.setHeader("Allow", httpMethods.join(", "));
  rsp.setHeader("Vary", "Allow");
  srcs = ['.']; //correct later.
  httpMethod = function() {return Promise.resolve()};
  }
 
 if (bAllowed)
 echo(
  "\r\nApplying HTTP method " + rq.method + " to resource '" + rq.url
  + "', then will set response code and response headers, then will decide, whether to send body at all"
  + ", then will validate response headers, then, maybe, just maybe will write response body."
  );
 
 else echo(
  "\r\nHTTP method "+rq.method+" unallowed or inapplicable to resource '" + rq.url
  + "'\r\nAllowed methods are: " + httpMethods.join(", ") + "."
  );
 
 //"HTTP method implementations", as defined, receive "settings for path".
 //I wonder, whether "settings for path" and underResource should be crammed into an array governed by the true SFP to pass to searchUpPathAndWrite()...
 
 function applyHttpMethodToSources(method, srcs, more, rsp, sfp) {
  var tailDecoded = decodeURIComponent(sfp.firstKeyTail)
  var promToApply = method(
   [srcs[0], tailDecoded],
   more,
   rsp,
   sfp
   );
  promToApply = promToApply.catch(
   function(er) {
    if (srcs.length >= 2) {
     srcs.shift();
     return applyHttpMethodToSources(method, srcs, more, rsp, sfp);
     }
    else return Promise.resolve(
     {payloadType: 'nullDevice', code: 403}
     )
    .then(er.finaliseHttpResponsePreparation);
    
    }
   );
  return promToApply;
  }
 
 return (
  applyHttpMethodToSources(httpMethod, srcs, rq, rsp, sfp)
  //searchUpPathAndWrite(srcs, sfp, rsp, rq, httpMethod)
  .then(setResponseCode).then(setResponseHeaders).then(decideSendBody).then(validateResponseHeaders)
  .then(maybeWriteResponseBody)
  .then(
   function(resolved) {
    //cache sfp.isNull to assign that to other SFP-s using the same .firstKey:
    sfpNullishPropsCache[sfp.firstKey] = sfp.isNull;
    return resolved;
    }
   )
  );
 
 function setResponseCode(unused) {
  echo("\r\nLet us try to get response code.");
  
  //while (sfp.keys.length > 0) sfp.unget();
  underResource.traverseKeysInSfpChain([], -1, false); //to root.
  
  rsp.statusCode = //delegateMod.code instanceof Function && delegateMod.code() ||
  underResource.get("code") || 200; //default status code should be '200'.
  echo("\r\nHas set status code to "+rsp.statusCode+" eventually.\r\n");
  //rsp.writeHead(); //writeHeader()?
  
  //echo("\r\nI want to unget(), damn you!");
  //echo("underResource.current: " + otos(underResource.current) + ".");
  echo("\r\nexiting .code...");
  underResource.unget();
  echo("\r\nexited .code.");
  //debug:
  //echo("underResource.current: " + otos(underResource.current) + ".");
  
  return unused;
  }
 
 function setResponseHeaders(unused) {
  echo("\r\nWhich HTTP headers to respond with?");
  
  //to 'headers':
  underResource.traverseKeysInSfpChain(['headers'], -1, false);
  
  headerNames = underResource.getKeys(); //keys (header names) in 'headers'.
  echo("\r\nWill respond with HTTP headers: "+headerNames.join(", ")+".");
  
  for (i = 0; i < headerNames.length; i++) {
   headerName = headerNames[i];
   headerName = headerName.toLowerCase();
   if (rsp.hasHeader(headerName)) echo("Has already set '"+headerName+"'.");
   else {
    //has not yet set it.
    //avoid 'getinstance':
    if (headerName !== 'getinstance')
    header = underResource.get(headerName);
    else header = {};
    
    //gotten header name, validate:
    echo("\r\nWould set "+headerName+": "+header+".");
    
    //allow only strings, remove line-folding:
    if (typeof header === 'string' || header instanceof String) {
     header = header.replace(/[\r\n]/g, "");
     echo("\r\nWill set "+headerName+": "+header+".");
     rsp.setHeader(headerName, header);
     }
    
    //if header name not string, try conversion.
    //got no 'getinstance', thus conversion will convert the headers object itself. Avoid that!
    else if (headerName === 'getinstance') {
     echo('That would be unwise, though. Will not.');
     }
    //nullish values can't be converted, because no .toString():
    else {
     if (header == null) {
      //misconfig., report internal server error:
      echo("\r\nHeader '"+headerName+"' is null!");
      return Promise.reject("\r\nHeader '"+headerName+"' is null!");
      //returning a non-null Promise-resolve value signifies an internal server error.
      //But only in response writer!
      //header = header || String(header) && 
      }
     else {
      //can convert, do:
      header = header.toString().replace(/[\r\n]/g, "");
      echo("Will set "+headerName+": "+header+".");
      rsp.setHeader(headerName, header);
      }
     }
    
    //return .current to 'headers', unless 'getinstance':
    if (headerName !== 'getinstance') underResource.unget();
    }
   
   //in addition to the header that's been set -- correct "Connection":
   if (headerName == "Upgrade") rsp.setHeader("Connection", "upgrade");
   
   } //end of for each header name.
  
  return unused;
  } //end of func.
 
 function decideSendBody(unused) {
  
  underResource.traverseKeysInSfpChain(['body'], -1, false);
  
  if (sendBody) {
   //if (handler != null)
   if (underResource.current.myValue != null) sendBody = underResource.get("sendBody");
   //if has not set sendBody to 'false' explicitly, it must not change and stay 'true'.
   if (sendBody !== false) sendBody = true;
   }
  else {
   //if (handler != null) sendBody = underResource.get("sendBody");
   if (underResource.current.myValue != null) sendBody = underResource.get("sendBody");
   //if has not set sendBody to 'true' explicitly, it must not change and stay 'false'.
   if (sendBody !== true) sendBody = false;
   }
  
  return unused;
  }
 
 function validateResponseHeaders(unused) {
  //certain codes imply certain headers:
  
  if (rsp.statusCode > 99 && rsp.statusCode < 200 || rsp.statusCode == 204) {
   //must not set Transfer-Encoding, and so must not Content-Length.
   if (rsp.hasHeader("Transfer-Encoding")) rsp.removeHeader("Transfer-Encoding");
   if (rsp.hasHeader("Content-Length")) rsp.removeHeader("Content-Length");
   
   //if 101, must set Upgrade, order bottom to top, obtain values from request's Upgrade.
   //Otherwise report misconfig..
   echo("Have set status code to "+rsp.statusCode+", will not send body.");
   sendBody = false;
   }
  
  if (rsp.statusCode == 205) {
   //If responding with 205 Reset content, the server must not send body. Instead it must either set C-L to zero, or rely on "Transfer-Encoding: chunked", or close the connection after Response.end().
   rsp.setHeader("Content-Length", "0");
   echo("Have set status code to "+rsp.statusCode+", will not send body.");
   sendBody = false;
   }
  
  if (rsp.statusCode == 206) {
   //must set Content-Range.
   }
  
  if (rsp.statusCode == 304) {
   echo("Have set status code to "+rsp.statusCode+", will not send body.");
   sendBody = false;
   }
  
  if (rsp.statusCode == 401) {
   //must set WWW-Authenticate.
   }
  
  if (rsp.statusCode == 405) {
   //method not allowed. Must include: Allow: <list>.
   }
  
  if (rsp.statusCode == 426) {
   //must set Upgrade, order most accepted by server to least accepted.
   }
  
  return unused;
  }
 
 function maybeWriteResponseBody(unused) {
  //if (delegateMod.body instanceof Function) delegateMod.body();
  //Before sending body confirm it needs to be sent:
  if (!sendBody) {
   //echo("\r\n.handler.sendBody is 'false', will not bother.");
   return Promise.resolve(); //No content.
   }
  
  //now comes...
  //Extract rules determining response's body and write the body to response:
  
  underResource.traverseKeysInSfpChain(['body'], -1, false);
  //to root, then to 'body' and, finally, to wrw.
  wrw = underResource.get("writeResponseWith");
  
  if (wrw instanceof Array) {
   responseWriterArg = wrw[0];
   responseWriter = wrw[1];
   }
  
  /*if (responseWriterArg instanceof Function || responseWriterArg == null) {
  responseWriterArg = sfp.get('0');
  sfp.unget(); //exit '0', return to .writeResponseWith;
  }
  */
  
  //if (responseWriterArg == null) responseWriterArg = 'utf8';
  
  echo("underResource.currentKey ('"+underResource.currentKey+"') is expected to be 'writeResponseWith'.");
  
  underResource.defaultKeyi = 0;
  
  while (responseWriter instanceof Function === false && underResource.defaultKeyi < underResource.defaultKeys.length) {
   fbkWrw = underResource.getDefault(underResource.currentKey); //returns a Wrapper!
   if (fbkWrw != null) {
    fbkWrw = fbkWrw.myValue;
    if (fbkWrw instanceof Array) responseWriter = fbkWrw[1];
    }
   //underResource.unget(); //exit '1', return to .writeResponseWith.
   }
  
  if (responseWriter instanceof Function === false)
  responseWriter = function() {
   echo("\r\nserver.js: no responseWriter defined.");
   return Promise.resolve();
   };
  
  //возможно, как раз здесь и стоит передавать не сфп, а ундерРесоурце.
  //Это может быть полезно для ХЕАД-а, пкку респонсеВвритер() может переопределить заголовки.
  
  //even though responseWriter is no HTTP method, it abides by similar rules.
  return applyHttpMethodToSources(responseWriter, srcs, responseWriterArg, rsp, sfp);
  
  //searchUpPathAndWrite(srcs, sfp, rsp, responseWriterArg, responseWriter);
  
  } //end of func.
 
 //HTTP spec. conformance.
 //If method name is longer, than implemented, send 501 Not implemented.
 //Long URI-s the server wishes not to process must be answered with 414 URI too long.
 //(v) Headers must not include "\r\n " (line-folding). Before setting header values they must be validated.
 //If the server wishes not to process large headers, it must send a client-error code.
 //Must check "Transfer-Encoding: chunked" and not add "chunked" more, than once.
 //If code is 1xx or 204, either remove the set Transfer-Encoding, or that is an error, in which case
 //the settings must be modified.
 //If CONNECT is responded with 2xx, disallow Transfer-Encoding, see above.
 //Must not use Transfer-Encoding, unless serving request to client prior to HTTP/1.1.
 //If request send unfamiliar Transfer-Encoding, the server should respond with 501 Not implemented.
 //Must not set both Transfer-Encoding, and Content-Length.
 //Disallow Content-Length in HEAD, unless check is configured to be done. Or perform such a check in any case, if Content-Length is configured for HEAD. That is, C-L must reflect the actual length, even though there is no body.
 //If conditional GET, and status code set to 304 Not modified, then check the actual length (when response code would have been set to 200).
 //If code 1xx or 204, remove Content-Length. If CONNECT and code 2xx, remove C-L.
 //The server should measure length and set it before sending headers and thus before writing to response.
 //If code 1xx, 204 or 304, then only set headers.
 //If method HEAD, then only set headers.
 //If request has Transfer-Encoding, but its final value is not "chunked", then send 400 Bad request and close connection.
 //If request has not Transfer-Encoding, but C-L is invalid somehow, then send 400 Bad request and close connection.
 //The server must handle absolute URI-s. That is it must check the scheme and the Host header.
 //If received HTTP/1.1 and: no Host header, or multiple headers, or invalid Host header -- then send 400 Bad request.
 //If the server supports not presistent connections and responds with any code but 1xx, it must send "Connection: close".
 //If received "Connection: close", then after responding to that and calling .end() on Response object
 //the server must close connection (not stop listening).
 //If the server send "Connection: close", it must close it as above.
 //If sending 101, must also send the Upgrade header. If cannot, report misconfiguration. Must list values in "Upgrade" in layer-ascending order (from bottom layer to top one). Must obtain values from request's "Upgrade".
 //If sending 426 Upgrade required, must also set "Upgrade", listing protocols in order of preference
 //(use protocol 1 as the most accepted; use the last one as the least accepted).
 //If new protocol cannot honour message semantics, must not switch protocols (must not send 101). All protocols are said to be able to honour OPTIONS.
 //If sending "Upgrade", must also send "Connection: upgrade".
 //If client uses HTTP/1.0, request header "Upgrade" must be ignored.
 //"Expect" header must be processed before "Upgrage": "Expect: 100-continue" must be answered with 100 Continue.
 //
 //When encoding repres., must set Content-Encoding; multiple encodings must be listed in order of their
 //application by the server.
 //The server must support at least GET and HEAD.
 //If URI, as configured, implies performing unsafe actions, then request method must not be safe.
 //The server must not send body to HEAD requests.
 //If PUT delegate create a resource, must respond with 201 Created. If modified existing, send either 200, or 204.
 //The server must not set Etag or Last-Modified in response to PUT. Exception: the new repres. differs not from repres. received in the PUT request.
 //PUT must be applied to the target URI. Other URI-s must not be affected by that. The server must send
 //3xx to change the URI.
 //If PUT for an URI is allowed, request header "Content-Range" accompanying PUT must be answered with 400.
 //If CONNECT is responded with 2xx, neither Transfer-Encoding, nor Content-Length must be sent.
 //The server must set C-L in response to OPTIONS, even when there is no body to respond with.
 //If the client uses HTTP/1.0, the server must ignore "Expect: 100-continue".
 //Sending 100 must be followed (eventually) by sending any code but 1xx.
 //If received HTTP/1.1 and "Expect: 100-continue", as well as indications, that request body shall follow, then the server must either try to respond with a final status code, or send 100.
 //The server must only generate qvalues, if at all, with 3 digits or less after dec. point.
 //If the client uses HTTP/1.0, THE SERVER must not send 100.
 //The server must set "Upgrade", if will respond with 101, otherwise it must report misconfig..
 //If responding with 205 Reset content, the server must not send body. Instead it must either set C-L to zero, or rely on "Transfer-Encoding: chunked", or close the connection after Response.end().
 //If responding with 405 Method not allowed, the server must set Allow header and list allowed methods
 //for the target resource.
 //If sending 426 Upgrade required, the server must also set Upgrade header or otherwise report miconfig..
 //The server must have a clock to verify Date header it sends, unless sending 1xx or 5xx.
 //
 //Last-Modified must not be a date, later, than Date header. Corrections should be made, and L-M should
 //be set to Date.
 //If the server has no clock, L-M must not be set.
 //Entity-tag provision must be conformant, see RFC 7232.
 //In conditional GET-s before applying the method request headers must be checked.
 //In certain cases the server must ignore If-Modified-Since/If-Unmodified-Since.
 //If the server answers with 200 and sets any of: Cache-Control, Content-Location, Date, ETag, Expires and Vary -- it must also set headers from that same set, if answering with 304.
 //If eventual status code is 2xx or 412 Precondition failed, preconditions may be checked before performing the request method. Otherwise the server must ignore such preconditions.
 //Range header may be honoured only by GET.
 //When answering 206 Partial content, the server must set Content-Range.
 //If any of: Date, Cache-Control, ETag, Expires, Content-Location and Vary -- are sent in response with 200, they must as well be sent in response with 206.
 //Code 401 must be accompanied with WWW-Authenticate.
 
 //put 'break', when each case ends.
 switch (oUrl.pathname) {
  case '/':
  rsp.writeHead(
   200, {"Content-Type": "text/html; charset=UTF-8"}
   );
  return writeFromToSafely('site/home.html', 'utf8', rsp);
  
  break;
  
  //case '/favicon.ico':
  //give away the icon.
  //case '/help':
  //serve help.
  //case '/history':
  //serve history. But don't serve his Tori. Y'know... Asgore's Tori! ["rimshot"].
  //case '/main':
  //perform main action.
  break;
  case '/source': ;
  console.log('\r\nRequested "/source".');
  rsp.writeHead(
   200, {"Content-Type": "text/plain; charset=UTF-8"}
   );
  //calling writeFromTo intentionally! Don't erase .then!
  return writeFromTo(
   process.argv[1]
   , 'utf8', rsp)
  .then(serverResponseWritten, onErrorWritingResponse);
  
  case '/testtemplate':
  //debugger;
  echo("/testtemplate: call mergeFiles().");
  rsp.setHeader('Content-Type', "text/plain; charset=UTF-8");
  return sendWebPage('site/home.html', rsp);
  default:
  rsp.writeHead(
   501, {"Content-Type": "text/html; charset=UTF-8"}
   );
  return writeFromToSafely('site/service_unavailable.html', 'utf8', rsp);
  
  }
 return Promise.resolve();
 }

function /*['']*/lineUpSourcesWith(/*[{}]*/inSrcs, /*''*/pathname) {
 var outSrcs = [], srco, src, termi, hasPushed = {};
 for (var i = 0, l = inSrcs.length; i < l; i++) {
  srco = inSrcs[i];
  termi = pathname.indexOf(srco.replaceWhat);
  
  //if srco.replaceWhat cannot be found in pathname, let us just assume pathname must be replaced whole:
  //(or, maybe, necessitate the equation: termi + srco.replaceWhat.l == 0)
  //(or, maybe, make it equal the first key...)
  //(...which we don't even know...)
  //(but the idea is good!)
  //
  //(In all actuality, we do know the first key: it's 'pathname'! So there!..)
  if (termi < 0) termi = pathname.length - srco.replaceWhat.length;
  
  //suppose, 'pathname' (the first key) has .replaceWhat (that'll be replaced).
  //The part after .replaceWhat will be appended to .replaceWith.
  //'pathname' may fall back on some of .replaceWhat-s.
  //What, if the fallback is nowhere to be found? Who is the closest to inherit?
  //(SFP).defaultKeys may have that information!
  
  src = pathMod.join(
   srco.replaceWith,
   decodeURIComponent( //this part may be url-encoded. Ixnay!
    pathname.slice(termi + srco.replaceWhat.length)
    )
   );
  //inSrcs may contain duplicates.
  echo("Has pushed source '"+src+"'?");
  if (!hasPushed[src]) {
   echo("No, will push.");
   outSrcs.push(src);
   hasPushed[src] = true;
   }
  else echo("Yes.");
  } //end of for.
 return outSrcs;
 }

function /*[{}]*/unifySources(/*SettingForPath*/settings, /*['']*/aWhere) {
 echo("\r\nunifySources(): settings.currentKey is '"+settings.currentKey+"' (expected: either the first key, or its fallback).");
 var src, srcs, srcsVal, typeMod = require('type');
 var isArrayLike = typeMod.isArrayLike;
 var srcVal;
 
 if (!isArrayLike(aWhere)) aWhere = settings.keysToMyCurrent();
 
 srcs = [];
 echo("\r\nunifySources(): so far srcs has: "+srcs.join(", ")+".");
 
 var fbks = settings.defaultKeys.slice(0), fbki, firstKey = settings.firstKey, parent, body
 ,bodySrcs, hasPushed = {}, hasPushedVal, bodyVal;
 var whichKey, sfpChain, underKeys, usrcs = [], usfpw, usfp, thisOrHelperSfp;
 
 //forget not to mark values as pushed!
 for (var j = 0; j < srcs.length; j++) {hasPushed[String(srcs[j])] = true}
 
 fbks[-1] = settings.firstKey;
 
 for (fbki = -1; fbki < fbks.length; fbki++) {
  echo("\r\nWill examine '"+fbks[fbki]+"'.");
  //settings.firstKey = fbks[fbki];
  /*Wrapper|SFP*/body;
  sfpChain = settings.traverseKeys(fbks[fbki], aWhere.slice(1), 2);
  usfpw = sfpChain[0].current;
  if (usfpw instanceof SFPwrapper) {
   //must recur.
   //underKeys = sfpChain[0].supremeKeysToUnderKeys(sfpChain[1], aWhere);
   //validate usfp:
   usfpw.unwrap();
   usfp = usfpw.unwrapTopLayer();
   //then recur:
   bodySrcs = unifySources(usfp, usfp.keysToMyCurrent());
   } //end of if recursion.
  else {
   body = usfpw;
   thisOrHelperSfp = sfpChain[0];
   //ditch total unwrapping:
   //if (body) bodyVal = body.myValue;
   //instead provide dummy value:
   bodyVal = true;
   
   if (body) {
    //body = body.myValue;
    
    src = thisOrHelperSfp.cd('src'); //supreme SFP ('thisOrHelperSfp') has not budged!
    
    //But SFPwrapper fixes that.
    //debug:
    //echo("'"+fbks[fbki]+"': src.underKeys is: "+otos(src.underKeys)+".");
    //src.underKeys.push('src'); //(Obsolete) So push 'src' into src.underKeys!
    
    if (src) srcVal = src.myValue;
    
    if (src && srcVal) {
     src = srcVal;
     hasPushedVal = (hasPushed[String(src)] || false);
     echo(
      "\r\nHas pushed '"+String(src)+"'? "
      //+ hasPushedVal+"."
      );
     if (!hasPushedVal) {
      srcs.push({replaceWhat: fbks[fbki], replaceWith: src});
      //the caller will have to deal with duplicates.
      //hasPushed[String(src)] = true;
      }
     }
    else {echo("\r\nunifySources(): '"+fbks[fbki]+"': src is "+(src && srcVal)+".")}
    
    bodySrcs = thisOrHelperSfp.unget().cd('srcs'); //same problem with supreme SFP!
    //Not any more.
    //bodySrcs.underKeys.push('srcs');
    if (bodySrcs != null) bodySrcs = bodySrcs.myValue;
    //echo("'"+fbks[fbki]+"': body.get('srcs').myValue is: "+otos(bodySrcs)+".");
    if (isArrayLike(bodySrcs)) {
     } //end of if.
    else echo(
     "\r\nWARNING: unifySources(): ignoring settings['"+fbks[fbki]+"'].body.srcs, because it is "
     + bodySrcs
     + ", not an array!"
     );
    thisOrHelperSfp.unget();
    }
   } //end of else no recursion.
  
  //merge the sources collected in this iteration with the resulting sources:
  //(expect bodySrcs to be an array of maybe-strings)
  //(in case of a recursive call bodySrcs is an array of objects)
  for (var srco, j = 0, l = (isArrayLike(bodySrcs)? bodySrcs.length: j); j < l; j++) {
   srco = bodySrcs[j];
   
   if (typeof srco === 'string' || srco instanceof String || (srco instanceof Object === false))
   src = String(srco);
   else src = srco.replaceWith;
   
   hasPushedVal = hasPushed[src] || false;
   echo(
    "\r\nHas pushed '"+src+"'? "
    //+hasPushedVal+"."
    );
   if (!hasPushedVal)
   {
    srcs.push({replaceWhat: fbks[fbki], replaceWith: src});
    //the caller will handle duplicates.
    //hasPushed[src] = true;
    }
   } //end of for.
  
  } //end of for.
  
 //settings.firstKey = firstKey;
 echo("\r\nLeaving unifySources(), will return the following sources: "+srcs.join(", ")+".");
 return srcs;
 }

function /*Promise*/ putContentIntoFile(destFilePath, encoding, serverResponse, contentStream) {
 echo("Let us put content into a file!");
 //access mode: setUID setGID sticky | ownR ownW ownEx | groupR groupW groupEx | otherR otherW otherEx
 var localFile = fs.createWriteStream(destFilePath, {flags: 'w', encoding: encoding, mode: 256+128});
 echo("Created write stream for "+destFilePath+".");
 //localFile.on('close'); //descriptor closed.
 //localFile.on('open', (fileDesc)=>{}); //file opened.
 //If internal buffer full, wait for it to drain and try writing again.
 //if (!localFile.write()) localFile.once('drain', ()=>{localFile.write()});
 //localFile.on('error', () => Promise.resolve().then(handleErrors).then(closeFile))
 //Call .end() and wait for 'finish':
 //localFile.on('finish', ()=>{echo('all flushed to disk.')})
 //Call someStream.pipe(localFile) to emit:
 //localFile.on('pipe', (piperStream)=>{})
 //Same for 'unpipe' event.
 //
 //Call localFile.write().
 //Disallow further writes with localFile.end().
 
 return new Promise((onFul, onBroken)=>{
   localFile.on('close', ()=>{echo("Descriptor closed for "+destFilePath+".");}); //descriptor closed.
   localFile.on('open', (fileDesc)=>{echo("File "+destFilePath+" opened, descriptor: "+fileDesc+".")}); //file opened.
   localFile.on('error', onBroken);
   localFile.on('finish', onFul);
   localFile.on('pipe', (producer)=>{
     echo("Received 'pipe' event, connected 'producer': (unmentioned here).");
     //echo(otos(producer));
     }
    );
   
   //Pipe data from contentStream to localFile and, when contentStream end, end localFile as well:
   echo("Let us pipe from "+contentStream.path+" to "+destFilePath+"!")
   promiseToPipe(contentStream, localFile).then(
    ()=>{echo(""+contentStream.path+" has ended, let us end "+destFilePath+"!"); localFile.end()},
    onBroken
    );
   contentStream.on('data', (chunk)=>{echo('Has read data chunk: '+chunk+'')});
   }
  );
 }

function
/*Promise to pipe stream named "from" to "to"*/
writeFromTo(
 /*String*/from, fromEncod, /*WritableStream*/to
 )
{
 var fileDataStream = fs.createReadStream(
  from,
  {
   flags:'r', encoding: fromEncod, autoClose: true, start: 0
   }
  );
 console.log('writeFromTo(): Created read stream for "'+from+'" (in '+fromEncod+')');
 return promiseToPipe(fileDataStream, to).then(function() {return from});
 };

//Remember: writeFromToSafely() takes its first arg relatively to rootDir!
function
/*Promise to write from stream named fromRoot to "to"*/
writeFromToSafely(/*String*/fromRoot, fromEncod, /*WritableStream*/to) {
 return writeFromTo(
  pathMod.join(rootDir, fromRoot)
  , fromEncod, to)
 .then(serverResponseWritten, onErrorWritingResponse)
 }

function serverResponseWritten(fullPathWrittenFrom) {
 console.log('Response written from file "'+fullPathWrittenFrom+'".')
 ;};
function onErrorWritingResponse(er) {
 //maybe set header to error code.
 echo(
  '\r\nCaught an error!\r\n' + otos(er)
  );
 echo(er);
 //debugger;
 echo("onErrorWritingResponse(): Me no handle errors!");
 if (!(er instanceof Error)) er = new Error(er);
 return Promise.reject(er);
 };

var sps = require('server/settingsPerSite');
var SettingsForPath = sps.SettingsForPath;
var settingsForPath = sps.settingsForPath;

function sendWebPage(relpath, encod, rsp, masterPageDef) {
 //maybe, pass whatever be in masterPageDef (in future).
 echo("sendWebPage(): let us merge the template file and "+relpath+".");
 return mergeFiles(
  [pathMod.join(rootDir, 'site/template.html'), pathMod.join(rootDir, relpath)],
  {
   0: '[0]['+placeholderHeadPtrn+'][0]:[1][(<head>)*(</head>)][0]',
   1: '[0]['+placeholderBodyPtrn+'][0]:[1][(<body>)*(</body>)][0]',
   'tail': 0
   }
  )
 .then(
  function(mergedFile) {
   echo('sendWebPage(): got merged file, proceed to piping it to server response.');
   //maybe, there are no data yet? Do check.
   mergedFile.on(
    'data',
    function(data) {
     echo('Have read some data from the merged file, proceed to writing them to server response.');
     //echo('Data: "'+data+'".');
     rsp.write(data);
     }
    );
   return new Promise(
    function(onFul, onBroke) {
     mergedFile.on('end', onFul);
     mergedFile.on('error', onBroke);
     }
    )
   //return promiseToPipe(mergedFile, rsp);
   }
  //if errors happen, before we get the merged file:
  , onErrorWritingResponse
  )
 }

var placeholderHeadPtrn = '<!--head-placeholder-->',
placeholderBodyPtrn = '<!--body-placeholder-->';

//how to read files:
/*const readable = require('fs').createReadStream(
 'path',
 {
  flags:'', encoding:'', fd: Number(''), mode: Number(''), autoClose: true, start: Number(''), end: Number(''),
  highWaterMark: Number('')
  }
 );
readable.on('data', (chunk) => {
  console.log(`Received ${chunk.length} bytes of data.`);
});
readable.on('end', () => {
  console.log('There will be no more data.');
});
//there may be errors:
//readable.on('error', function(er) {});
*/
//if encoding is not set, Buffers will arrive.

//Additionally:
//readable.pipe(writable, {end: false}); //do set 'end' to false, for we want not writable to be closed prematurely!
//There may be: readable.unpipe(writable);

//expressApp.get(
//'/admin', function(rq, rslt, next)
function getAdminPage(rq, rslt, next)
{
 rslt.sendfile(
  pathMod.join(__dirname, 'admin.html')
  );
 }
//);
//expressApp.get(
//'/explorer',
function getExplorerPage(rq, rslt, next)
{
 var fs = require("fs");
 var rqQueryDir = decodeURIComponent(rq.query.dir || '/');
 console.log(
  '\r\nQuery string: '+objToString(rq.query) +
  '\r\ndir = '+rqQueryDir+'');
 
 //Обработать случай с виртуальными каталогами.
 var actualDir = pathMod.join(__dirname, rqQueryDir);
 for (var i = 0; i < virtualDirs.length; i++)
 {
  if (
   rqQueryDir.indexOf(
    virtualDirs[i]
    [0]
    )
   === 0)
  {
   //if (rqQueryDir.length < virtualDirs[i][0].length) continue;
   console.log(rqQueryDir + ' seems to be a virtual path.');
   actualDir = pathMod.join(
    virtualDirs[i]
    [1],
    rqQueryDir.slice(
     virtualDirs[i]
     [0].length
     )
    );
   console.log('Actual directory would be "'+actualDir+'".');
   break;}
  }
 fs.readdir(
  actualDir,
  function(er, /*[file names]
   */
   files)
  {
   if (er != null)
   {
    console.log(
     '\r\nAn error occurred, while trying to read directory:\r\n' +
     '"' + actualDir +
     '"\r\nError: ' + objToString(er)
     );
    if (er.code == 'ENOTDIR')
    {
     console.log('error trying to read a non-dir by readdir().');
     console.log('Assume a file here.');
     fs.readFile(
      actualDir, 'utf-8', function(er, fileContents)
      {
       if (er == null)
       {
        console.log('File read.');
        console.log('arguments: ' + arguments.length);
        var args = objToString(arguments);
        console.log('\r\nargs: \r\n' + args);
        console.log('type of args: ' + typeof args);
        console.log(
         '\r\nargs[1]: \r\n' + args[1]
         );
        //send plain text, if HTML.
        if (
         actualDir.slice(
          actualDir.lastIndexOf('.')+1
          )
         .match(
          '\bhtm(\b|l\b)'
          )
         != null) {
         console.log('Sending plain HTML.');
         rslt.send(fileContents, 200);
         //else disable possible markup first.
         }
        else {
         console.log('Replacing "<" and sending.');
         rslt.header('Content-Type', 'text/plain');
         //console.log('Before: ' + args[1]);
         //args = args[1].replace(/\</g, "&lt;")
         //console.log('After: ' + args);
         rslt.send(fileContents, 200);
         }
        }
       else rslt.send(
        er.toString()
        , 500);
       }
      );
     //never return, unless we want to send the response!
     //return;
     } //end of if.
    //return rslt.send(er.toString(), 500);
    }
   else {
    //в этом месте нужно ко списку файлов добавить виртуальные каталоги.
    virtualDirs.forEach(
     function(virtDir)
     {
      var dirName;
      console.log(
       '\r\nvirtualDirs.forEach(): Virtual directory "'+virtDir[0]
       +'".');
      //избрать те, которые находятся в запрошенном каталоге.
      if (
       virtDir[0]
       .slice(0, rqQueryDir.length)
       !== rqQueryDir)
      return;
      else dirName = virtDir[0].slice(rqQueryDir.length);
      console.log(
       'Sliced '+(rqQueryDir.length)
       +' symbols.\r\ndirName is "'+dirName+'".');
      if (!dirName || dirName === '/') return;
      else {
       if (
        dirName.indexOf('/')
        == 0) {
        dirName = dirName.slice(1);
        console.log("dirName starts with a '/'." +
         " Removing the '/'.");
        }
       if (
        dirName.indexOf('/')
        > -1)
       console.log('dirName contains a path delimiter, thus is skipped.');
       else files.push(dirName);
       }
      }
     );
    files.sort(
     function(fName1, fName2)
     {
      if (
       fName1.toLowerCase()
       < fName2.toLowerCase()
       )
      return -1;
      else if (
       fName1.toLowerCase()
       > fName2.toLowerCase()
       ) return 1;
      else return 0;
      }
     );
    //file names are to be html-encoded, wrapped in markup, joined nicely and then sent.
    rslt.send(
     files.map(
      function(fileName)
      {
       return '<tr><td><a>' + fileName.replace('<', '&lt;')
       .replace('&', '&amp;') + '</a></td></tr>';
       }
      )
     .join('\r\n'),
     200);
    }
   }
  );
 }
//);
